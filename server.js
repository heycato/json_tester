const http = require('http')
const fs = require('fs')
const path = require('path')
const jsonlint = require('jsonlint')
const getEncoding = require('detect-character-encoding')
const exec = require('child_process').exec;
const port = process.argv[2] || 8080
const view = require('./view.js')
const mimetypes = {
  '.js':'application/javascript'
}

const fetchJson = (url) =>
  new Promise((respond, reject) => {
    http.get(url, (res) => {
      if(res.statusCode !== 200) {
        reject('Could not get json')
      } else {
        let body = ''
        res.on('data', (chunk) => {
          body += chunk
        })
        res.on('end', () => {
          try {
            respond(JSON.parse(body))
          } catch(e) {
            let n = Number(e.toString().split(' ').pop())
            reject({msg:e, body, around:body.substring(n - 200, n + 200)})
          }
        })
      }
    })
  })

http.createServer((req, res) => {
  if(req.method.toLowerCase() === 'get') {
    if(req.url === '/') {
      res.writeHead(200, {'content-type':'text/html'})
      res.end(view())
    } else {
      let mimetype = mimetypes[path.extname(req.url)]
      fs.readFile(`.${req.url}`, (err, file) => {
        res.writeHead(200, {'content-type':mimetype})  
        res.end(file)
      })
    }
  } else {
    let body = ''
    req.on('data', (chunk) => body += chunk)
    req.on('end', () => {
      fetchJson(JSON.parse(body).url)
        .then((json) => {
          res.writeHead(200, {'content-type':'application/json'})
          res.end(JSON.stringify(json))
        })
        .catch((reason) => {
          let output = {
            'somewhere around here:': reason.around,
            error:reason.msg.toString()
          }
          res.writeHead(200, {'content-type':'application/json'})
          try {
            output.jsonlint = jsonlint.parse(reason.body)
          } catch(e) {
            output.jsonlint = e.toString()
            output.charEncoding = getEncoding(Buffer.from(e.toString()))
          }
          res.end(JSON.stringify(output))
        })
    })
  }
})
.listen(port, () => console.log('Listening on port', port))
