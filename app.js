;((win, doc) => {

  win.onload = () => {
    const container = doc.querySelector('#container')
    const input = doc.querySelector('#json-input')
    const submit = doc.querySelector('#json-input-submit')
    const display = doc.querySelector('#output-container')
    const identity = x => x

    const appendChildren = (parent, children) =>
      children.reduce((p, c) => {
        p.appendChild(c)
        return p
      }, parent)

    const buildDisplay = (json) => {
      display.innerHTML = ''
      appendChildren(display, Object.keys(json)
        .map((key) => {
          const label = document.createElement('div')
          label.appendChild(document.createElement('hr'))
          const xmp = document.createElement('xmp')
          if(key === 'charEncoding') {
            label.appendChild(document.createTextNode('could be this encoding:'))
            xmp.innerHTML = json[key].encoding
          } else {
            label.appendChild(document.createTextNode(key))
            xmp.innerHTML = json[key]
          }
          label.appendChild(xmp)
          label.appendChild(document.createElement('hr'))
          return label
        })
      )
    }

    const handleUrl = (url) => {
      const xhr = new XMLHttpRequest()

      xhr.onload = ({target}) => {
        const json = JSON.parse(target.response)
        console.log(json)
        buildDisplay(json)
      }
      xhr.open('post', '/')
      xhr.setRequestHeader('content-type', 'application/json')
      xhr.send(JSON.stringify({url}))
    }

    const handleInput = (input) => {
      if(input.value !== '') {
        handleUrl(input.value)
        input.value = ''
        input.focus()
      }
    }

    input.onchange = ({target}) => handleInput(target)
    submit.onclick = () => handleInput(input)
  }

})(window, document)
