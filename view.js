const jsdom = require("jsdom").jsdom
const window = jsdom().defaultView
const document = window.document
const domjs = require('./dom.js')(window, document)

const { button, code, div, head, input, script, title, xmp } = domjs

const appendChildren = (parent, ...children) =>
  children.reduce((parent, child) => {
    parent.appendChild(child)
    return parent
  }, parent)

appendChildren(
  document.head,
  title('json tester'),
  script({src:'app.js'})
)
document.body.appendChild(
  div({id:'container'},
    input({id:'json-input', attribute:{placeholder:'enter url to json'}}),
    button({id:'json-input-submit'}, 'submit'),
    div({id:'output-container'})
  )
)

module.exports = () =>
  `<!doctype html>${document.documentElement.outerHTML}`
